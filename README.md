## A simple example of Good and bad implementation of Inheritance and Polymorphism in Ruby

Recently, I saw from the wall of my Facebook friend a [super stupid implemnentation of Template Method pattern in Ruby](http://www.betterruby.com/learning-from-a-gang-in-ruby/).
Normally, using a dynamic typed language, you can get rid of a lot of design patterns, for example Abstract Factory, Simple Factory, Visitor, Command and ... well, you guess it, Template Method design pattern.

I illustrate my opinion in this article about Template Method design pattern.

### Bad implementation using Template Method design pattern
You can read in the [stupid article](http://www.betterruby.com/learning-from-a-gang-in-ruby/) about the self praising of the stupid author about the problem (correctly identified) and his solution (super stupid, to say the least).
You can see the code here: [Bad implementation](https://github.com/linhchauatl/rooms_demo/tree/master/bad_implementation) 

There are several problems with this approach: 
* Create a whole object hierarchy to eliminate diferent paths of processing using overriding (inheritance + polimorphism) is terrible for computing resources and object design.
* In languages that have only single inheritance, you are out of luck for sub classes of your Room class to inherite anything else with this implementation.
* Unused methods in the class **Room**, convoluted the object interface. Useless code is the code you have to avoid to write at all cost. However, in static typed languages such as C++ and Java, there are valid reasons that you must use abstract unused methods. The most critical reason is that you want to implement an object hierarchy that can be used as argument for functions. 

Example in Java:
```java

public float calculateRoomCost(Room room) {
  float roomCost = room.calculateTotalCost();

  float realRoomCost = 0.0;
  //do some tax or other fee calculation here with roomCost

  return realRoomCost;
}

```

You want to use base class Room as argument for `calculateRoomCost`, because later on you might want to write some thing such as these to use with concrete implementations
```java
  NormalRoom room = new NormalRoom(3, 5);
  calculateRoomCost(room) 

```
or

```java
  SuiteRoom room = new SuiteRoom(3, 5);
  calculateRoomCost(room) 

```

Dynamic typed languages such as Ruby, Python have no such worriness about argument types for functions/methods, so the whole idea of Using Object Hierarchy to circumvent different processing paths/specific behaviors in dynamic laguages is ... welll ... you guess it ... **SUPER STUPID** .

* This implementation still forces developer to think about how to instantiate object room with correct concrete implementation at runtime, based on input or context from somewhere. 

You can see in the code of the file [**travel.rb**](https://github.com/linhchauatl/rooms_demo/blob/master/bad_implementation/travel.rb), at runtime, if the room_type is passed from somewhere, for example 

```ruby
class TravelService
  class << self
    # Calculate travel expense based on room type, number of nights and minibar_items
    # Assuming that this component get room type, number of nights and minibar_items
    # from user input from GUI, or from API call
    def calculate_expense(room_type, nights, minibar_items)
     
      # In a bad implementation, how the fuck you are supposed to know what 
      # sub class of Room must be instantiated here base on the parameter room_type 
      # passed as String 'Normal' or 'Suite' from screen.
      # In static typed languages, you would have to resort to 
      # if-else, switch-case or dependency injection.
      room = WhatTypeOfFuckingRoom.new(nights, minibar_items)
      room.calculate_total_cost
    end
  end
end
```

In static typed language, for example Java, people have to resort to one of the following: 
* if-else, switch-case in the code
* Dependency injection (Favorite of several design patterns, and the Spring framework implementation). 
* Configuration mapping (used alone or combined with Dependency injection).

There are rare techniques that a majority of Java developers don't know:
* Using HashMap to map between string type and Class.
* in Java 8, they introduce Lambda, to imitate dynamic features of dynamic languages.

Good news is that in dynamic languages such as Ruby, you have no problem with how to determine the concrete class to instantiate. But if you know Ruby well enough, you would never paint yourself into this rabbit hole in the first place.

**Extensibility**: 
* Now you dumb fuck have only the costs to worry about.
But imagine this: If you have DeluxeRoom and SuiteRoom with different room charges, but the same lighting equiments you want to maintain, but the lighting is different from NormalRoom.<br/>
How the fuck will you maintain the lightings? Duplicate code in the 2 classes DeluxeRoom and SuiteRoom, with the same name **lighting_maintenance** ?<br/>
Or you define a different hierarchy for the maintenance? Or what, dumb ass?



### Good implementation using Ruby mixin
You can see the code here: [Good implementation](https://github.com/linhchauatl/rooms_demo/tree/master/good_implementation)

This implementation leverage two feature/principle in Ruby, and any dynamic typed language:
* Dynamic object configuration at runtime.
* Prefer convention over configuration.

If you read this good implementation and still don't recognize all of its benefits, please go back to kindergarten, study Computer Science carefully, especially the following things:
* Object Oriented Design.
* Object Oriented Programming, concepts and principles.
* Design Patterns.
* Programming Language Theory.

