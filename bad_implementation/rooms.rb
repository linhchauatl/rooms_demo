# Here you have a unwieldy object hierarchy just to calculate the cost of a room.

class Room  
  attr_reader :nights, :minibar_items

  def initialize(nights, minibar_items)
    @nights = nights
    @minibar_items = minibar_items
  end

  def calculate_total_cost
    "$#{calculate_room_charge + calculate_minibar_charges + calculate_parking_charges}"
  end

  def calculate_room_charge
    raise 'You must implement calculate_room_charge in concreate class'
  end

  def calculate_minibar_charges
    raise 'You must implement calculate_minibar_charges in concreate class'
  end

  def calculate_parking_charges
    raise 'You must implement calculate_parking_charges in concreate class'
  end
end

class NormalRoom < Room

  def calculate_room_charge
    nights * 50
  end

  def calculate_minibar_charges
    minibar_items * 5
  end

  def calculate_parking_charges
    nights * 5
  end
end

class SuiteRoom < Room

  def calculate_room_charge
    nights * 90
  end

  def calculate_minibar_charges
    minibar_items * 3
  end

  def calculate_parking_charges
    nights * 2
  end
end