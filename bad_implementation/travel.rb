require_relative 'rooms'

class TravelService
  class << self
    # Calculate travel expense based on room type, number of nights and minibar_items
    # Assuming that this component get room type, number of nights and minibar_items from user input from GUI, or from API call
    def calculate_expense(room_type, nights, minibar_items)
      # In a bad implementation, how the fuck you are suppose to know what sub class of Room
      # must be instantiated here base on the parameter room_type passed as String from screen

      # Luckily, here is Ruby, you dumb fuck. Otherwise, you would have to resort to if-else, switch-case or dependency injection.
      room = eval("#{room_type}Room").new(nights, minibar_items)
      room.calculate_total_cost
    end
  end
end

puts "Cost for normal room in 3 nights with 5 items:  #{TravelService.calculate_expense('Normal', 3, 5)}"
puts "Cost for suite room in 3 nights with 5 items:  #{TravelService.calculate_expense('Suite', 3, 5)}"