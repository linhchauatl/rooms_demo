# Here you have just a room object, free to any behaviors and inheritances later.

module Normal
  def calculate_room_charge
    nights * 50
  end

  def calculate_minibar_charges
    minibar_items * 5
  end

  def calculate_parking_charges
    nights * 5
  end
end

module Suite
  def calculate_room_charge
    nights * 90
  end

  def calculate_minibar_charges
    minibar_items * 3
  end

  def calculate_parking_charges
    nights * 2
  end
end

class Room
   attr_reader :nights, :minibar_items

  def initialize(nights, minibar_items, room_type = 'Normal')
    self.class.include eval(room_type)
    @nights = nights
    @minibar_items = minibar_items
  end

  def calculate_total_cost
    calculate_room_charge + calculate_minibar_charges + calculate_parking_charges
  end
end
