require_relative 'rooms'

class TravelService
  class << self
    # Calculate travel expense based on room type, number of nights and minibar_items
    # Assuming that this component get room type, number of nights and minibar_items from user input from GUI, or from API call
    def calculate_expense(room_type, nights, minibar_items)

      # In a good implementation, a room is a room, all the same, so you don't have to worry about what concrete implementation
      # you have to use. And you don't have a whole object hierarchy to deal with in the first place.
      room = Room.new(nights, minibar_items, room_type)
      room.calculate_total_cost
    end
  end
end

puts "Cost for normal room in 3 nights with 5 items:  #{TravelService.calculate_expense('Normal', 3, 5)}"
puts "Cost for suite room in 3 nights with 5 items:  #{TravelService.calculate_expense('Suite', 3, 5)}"